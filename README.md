# symfony-messenger

[**symfony/messenger**](https://packagist.org/packages/symfony/messenger) [5708](https://phppackages.org/s/messenger) helps application send and receive messages to/from other applications or via message queues. https://symfony.com/messenger

# Documentation
## Unofficial documentation
* [*How to handle messages in batches with Symfony Messenger*
  ](https://wolfgang-klinger.medium.com/how-to-handle-messages-in-batches-with-symfony-messenger-c91b5aa1c8b1)
  2022-08 Wolfgang Klinger
* [*Send messages from Node app to Symfony using RabbitMQ*
  ](https://medium.com/@iyadh.faleh/send-messages-from-node-app-to-symfony-using-rabbitmq-a14611ab1b0e)
  2021-08 Iyadh Faleh
* [*How to use RabbitMQ with Symfony messenger*
  ](https://medium.com/@lsprezak/in-this-article-i-will-first-explain-how-the-queues-work-and-in-the-second-part-we-will-install-6e54039ffa9)
  2020-05 Łukasz
* [*An introduction to Symfony Messenger*
  ](https://jaxenter.com/symfony-messenger-167291.html)
  2020-02 Denis Brumann
* [*Symfony 4.4 Messenger Cheat Sheet*
  ](https://medium.com/@andreiabohner/symfony-4-4-messenger-cheat-sheet-5ca99cbba4a8)
  2019-12 Andréia Bohner

### Loosely coupled application components architectures
#### CQRS
* *Symfony Messenger component for CQRS applications*
  * [patryk.it
    ](https://patryk.it/symfony-messenger-component-for-cqrs-applications/)
    2020-09 Patryk Woziński
  * [dev.to
    ](https://dev.to/patrykwozinski/symfony-messenger-component-for-cqrs-applications-884)
    2020-09 Patryk Woziński

#### Hexagonal architecture
* (fr) [*Simplifiez vos formulaires Symfony*](https://www.choosit.com/blog/simplifiez-vos-formulaires-symfony/)
  2019-09 Gilles

